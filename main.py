from scraper_engine import *

SEARCH_ID = "574814"
ITEMS_PER_PAGE = "50"
TOTAL_ITEMS = 7905


def get_total_pages():
    return round(TOTAL_ITEMS / int(ITEMS_PER_PAGE))


def main():
    out_file = open("data.csv", "a")
    print("-"*50, "Comienza el scraper")
    total_pages = get_total_pages()
    print("*"*15, "TOTAL PAGES", total_pages)
    for page in range(1, total_pages + 1):
        print("="*25, "Current page", page)
        url = url_maker(2008, 2018, page, ITEMS_PER_PAGE, SEARCH_ID)
        print("Current URL --> ", url)
        html = get_html(url)
        soup = soup_maker(html)
        data = searcher(soup)
        for item in data:
            out_file.write("|".join(item) + '\n')
    print("-"*50, "Terminado")


if __name__ == '__main__':
    main()
