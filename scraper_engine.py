from bs4 import BeautifulSoup
import requests
import os

BASE_URL = "https://portal.issn.org/?q=api/search&"


def url_maker(start_date, end_date, page, items_per_page, search_id):
    url = BASE_URL + f"search[]=MUST=stadate={start_date}&search[]=MUST=enddate={end_date}&search[" \
                     f"]=MUST=srcname=ROAD&search_id={search_id}&currentpage={page}&size={items_per_page}"
    return url


def get_html(url):
    r = requests.get(url)
    return r.content


def soup_maker(data):
    return BeautifulSoup(data, 'html.parser')


def get_title(block):
    return block.find('div', class_='item-result-block-title').a.h5.string


def get_text(block):
    aux = []
    fields = block.find_all('p')
    for field in fields:
        aux.append(field.text)
    aux.pop()  # Removing trash
    return aux


def searcher(soup):
    """
    This function return clean data from one page of data
    :param soup:
    :return: string
    """
    data = []
    if soup.find('div', id='search-result-data'):
        data_items = soup.find_all('div', class_='item-result')
        for item in data_items:
            block = item.find('div', class_='item-result-block')
            title = get_title(block)
            text = get_text(block)
            text.append(f"Title: {title}")
            data.append(text)
        return data
